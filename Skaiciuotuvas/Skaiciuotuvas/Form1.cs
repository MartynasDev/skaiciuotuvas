﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Skaiciuotuvas
{
    public partial class Form1 : Form
    {

        int pagrindisnisSkaicius = 0;
        int antrasSkaicius = 0;
        bool arVeiksmas = false;
        int index = 0;
        bool nerasyk = false;
        public Form1()
        {
            InitializeComponent();
           
        }

        private void PagrindinisLangas_TextChanged(object sender, EventArgs e)
        {
            if (nerasyk==false)
            {
                if (arVeiksmas == true)
                {
                    antrasSkaicius = Convert.ToInt32(PagrindinisLangas.Text);
                    PagrindinisLangas.Text = antrasSkaicius.ToString();
                }
                else
                {
                    pagrindisnisSkaicius = Convert.ToInt32(PagrindinisLangas.Text);
                    PagrindinisLangas.Text = pagrindisnisSkaicius.ToString();
                }
            }
            else
            {
                nerasyk = false;
            }
           
        }

        private void Pliusas_Click(object sender, EventArgs e)
        {
            nerasyk = true;
            index = 1;
            arVeiksmas = true;
            PagrindinisLangas.Clear();
            

        }

        private void Minusas_Click(object sender, EventArgs e)
        {
            nerasyk = true;
            index = 2;
            arVeiksmas = true;
            PagrindinisLangas.Clear();
        }

        private void Daugyba_Click(object sender, EventArgs e)
        {
            nerasyk = true;
            index = 3;
            arVeiksmas = true;
            PagrindinisLangas.Clear();
        }

        private void Dalyba_Click(object sender, EventArgs e)
        {
            nerasyk = true;
            index = 4;
            arVeiksmas = true;
            PagrindinisLangas.Clear();
        }

        private void btn_lygu_Click(object sender, EventArgs e)
        {
            nerasyk = true;
            switch (index)
            {
                case 1:
                    int lygu = pagrindisnisSkaicius + antrasSkaicius;
                    PagrindinisLangas.Text = lygu.ToString();
                    pagrindisnisSkaicius = lygu;
                    break;
                case 2:
                    lygu = pagrindisnisSkaicius - antrasSkaicius;
                    PagrindinisLangas.Text = lygu.ToString();
                    pagrindisnisSkaicius = lygu;
                    break;
                case 3:
                    lygu = pagrindisnisSkaicius * antrasSkaicius;
                    PagrindinisLangas.Text = lygu.ToString();
                    pagrindisnisSkaicius = lygu;
                    break;
                case 4:
                    lygu = pagrindisnisSkaicius / antrasSkaicius;
                    PagrindinisLangas.Text = lygu.ToString();
                    pagrindisnisSkaicius = lygu;
                    break;
                default:
                    break;
            }
            arVeiksmas = false;
            //PagrindinisLangas.Clear();
        }

        private void Trinti_Click(object sender, EventArgs e)
        {
            if(arVeiksmas == true)
            {
                antrasSkaicius = antrasSkaicius / 10;
                PagrindinisLangas.Text = antrasSkaicius.ToString();
            }
            else
            {
                pagrindisnisSkaicius = pagrindisnisSkaicius / 10;
                PagrindinisLangas.Text = pagrindisnisSkaicius.ToString();
            }
        }
    }
}
