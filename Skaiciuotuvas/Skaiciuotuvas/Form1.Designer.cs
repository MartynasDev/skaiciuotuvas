﻿namespace Skaiciuotuvas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Pliusas = new System.Windows.Forms.Button();
            this.Minusas = new System.Windows.Forms.Button();
            this.Daugyba = new System.Windows.Forms.Button();
            this.Dalyba = new System.Windows.Forms.Button();
            this.Trinti = new System.Windows.Forms.Button();
            this.PagrindinisLangas = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_lygu = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Pliusas
            // 
            this.Pliusas.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Pliusas.Location = new System.Drawing.Point(74, 104);
            this.Pliusas.Name = "Pliusas";
            this.Pliusas.Size = new System.Drawing.Size(74, 51);
            this.Pliusas.TabIndex = 0;
            this.Pliusas.Text = "+";
            this.Pliusas.UseVisualStyleBackColor = true;
            this.Pliusas.Click += new System.EventHandler(this.Pliusas_Click);
            // 
            // Minusas
            // 
            this.Minusas.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Minusas.Location = new System.Drawing.Point(154, 104);
            this.Minusas.Name = "Minusas";
            this.Minusas.Size = new System.Drawing.Size(74, 51);
            this.Minusas.TabIndex = 1;
            this.Minusas.Text = "-";
            this.Minusas.UseVisualStyleBackColor = true;
            this.Minusas.Click += new System.EventHandler(this.Minusas_Click);
            // 
            // Daugyba
            // 
            this.Daugyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Daugyba.Location = new System.Drawing.Point(234, 104);
            this.Daugyba.Name = "Daugyba";
            this.Daugyba.Size = new System.Drawing.Size(74, 51);
            this.Daugyba.TabIndex = 2;
            this.Daugyba.Text = "*";
            this.Daugyba.UseVisualStyleBackColor = true;
            this.Daugyba.Click += new System.EventHandler(this.Daugyba_Click);
            // 
            // Dalyba
            // 
            this.Dalyba.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Dalyba.Location = new System.Drawing.Point(314, 104);
            this.Dalyba.Name = "Dalyba";
            this.Dalyba.Size = new System.Drawing.Size(68, 51);
            this.Dalyba.TabIndex = 3;
            this.Dalyba.Text = ":";
            this.Dalyba.UseVisualStyleBackColor = true;
            this.Dalyba.Click += new System.EventHandler(this.Dalyba_Click);
            // 
            // Trinti
            // 
            this.Trinti.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.Trinti.Location = new System.Drawing.Point(388, 104);
            this.Trinti.Name = "Trinti";
            this.Trinti.Size = new System.Drawing.Size(68, 51);
            this.Trinti.TabIndex = 4;
            this.Trinti.Text = "C";
            this.Trinti.UseVisualStyleBackColor = true;
            this.Trinti.Click += new System.EventHandler(this.Trinti_Click);
            // 
            // PagrindinisLangas
            // 
            this.PagrindinisLangas.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.PagrindinisLangas.Location = new System.Drawing.Point(74, 42);
            this.PagrindinisLangas.Multiline = true;
            this.PagrindinisLangas.Name = "PagrindinisLangas";
            this.PagrindinisLangas.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.PagrindinisLangas.Size = new System.Drawing.Size(382, 56);
            this.PagrindinisLangas.TabIndex = 5;
            this.PagrindinisLangas.TextChanged += new System.EventHandler(this.PagrindinisLangas_TextChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Skaiciuotuvas.Properties.Resources.new_CAB_PLUS2;
            this.pictureBox1.Location = new System.Drawing.Point(12, 161);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(587, 181);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btn_lygu
            // 
            this.btn_lygu.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(186)));
            this.btn_lygu.Location = new System.Drawing.Point(480, 104);
            this.btn_lygu.Name = "btn_lygu";
            this.btn_lygu.Size = new System.Drawing.Size(106, 51);
            this.btn_lygu.TabIndex = 7;
            this.btn_lygu.Text = "=";
            this.btn_lygu.UseVisualStyleBackColor = true;
            this.btn_lygu.Click += new System.EventHandler(this.btn_lygu_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 354);
            this.Controls.Add(this.btn_lygu);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PagrindinisLangas);
            this.Controls.Add(this.Trinti);
            this.Controls.Add(this.Dalyba);
            this.Controls.Add(this.Daugyba);
            this.Controls.Add(this.Minusas);
            this.Controls.Add(this.Pliusas);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Pliusas;
        private System.Windows.Forms.Button Minusas;
        private System.Windows.Forms.Button Daugyba;
        private System.Windows.Forms.Button Dalyba;
        private System.Windows.Forms.Button Trinti;
        private System.Windows.Forms.TextBox PagrindinisLangas;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_lygu;
    }
}

